/*****************************************************************************/
/* Mark Deegan                                                               */
/* Java socket client and server examples taken from:                        */
/* http://www.tutorialspoint.com/java/java_networking.htm                    */
/*****************************************************************************/

// we want everything to be in the ie.dit.mark package
package ie.dit.mark;


// needed for 
import java.net.*;
import java.io.*;

/** SimpleServer class creates a socket to connect to a server and pass a message */
public class SimpleServer extends Thread
{
	/** declare a single private member, the serverSocket */
	private ServerSocket serverSocket;
   
	/** one-parameter (int: port) constructor for this class */
	public SimpleServer(int port) throws IOException
	{
		// create a nre ServerSocket based on the assigned port number
		serverSocket = new ServerSocket(port);
		
		// set a timeout value of 10,000 seconds on this port
		// if we don't receive a client connection in that time, then stop waiting
		serverSocket.setSoTimeout(10000);
   } // end declaration of constructor

   /** instance method run for running the class SimpleServer */
   public void run()
   {
      // this will run forever, until we stop the program 
      while(true)
      { // start while loop
         try
         {
            // print Waiting message to the console indicating the server is waiting
            // for connections on the specified port
            System.out.println("Waiting for client on port " + serverSocket.getLocalPort() + "...");
            
            // await connections on the socket. This call blocks until we get a 
            // connection request on the serverSocket
            Socket server = serverSocket.accept();
            
            // when we do get a connection, print to the console that we are connected to
            // the remote socket
            System.out.println("Just connected to " + server.getRemoteSocketAddress());
            
            // create a DataInput Stream to receive data from the serverSocket
            DataInputStream in = new DataInputStream(server.getInputStream());
            
            // read the data from the DataInputStream and print to console
            System.out.println(in.readUTF());
            
            // create a DataOutputStream to send data via the serverSocket
            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            
            // write a "Thank you " message to the server Socket
            out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress() + "\nGoodbye!");
            
            // close the server socket
            server.close();
         } // close the try block
         
         catch(SocketTimeoutException s)
         { // start handling timeout exception, related to setSoTimeout call above
            // print a message to the console to the effect that the 
            // socket timed out waiting for a client connection
            System.out.println("Socket timed out!");
            
            // what is the effect of the break within a catch?
            break;
         }
         
         catch(IOException e)
         { // start catch
         
         	// print the exception that occurred
            e.printStackTrace();
                 
            // what is the effect of the break within a catch?
            break;
            
         } // end catch block
         
      } // end while(true) loop
   } // end declaration of method run
   
   /** main method will read the port number from the command -line,  
   create a thread based on the SimpleServer class, which causes the thread to use the
   run method declared above for the SimpleServer class */
   public static void main(String [] args)
   {
      int port = Integer.parseInt(args[0]);
      try
      { // start try block
         Thread t = new SimpleServer(port);
         t.start();
      } // end try block
      
      catch(IOException e)
      { // start catch block
         e.printStackTrace();
      } // end catch block
   } // end declaration of the main method for class SimpleServer
} // end declaration of class SimpleSever