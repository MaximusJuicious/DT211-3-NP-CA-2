/** all code should be developed as part of a package to ensure namespace
separation between many developers developing their own versions of utilities, classes and functions */
package ie.dit.deegan.mark;

/** Mark Deegan
Simple utility to print hello in Java
in Java */
public class Hello
{

	/** start declaration of main method */
	public static void main(String[] args)
	{
		// print the hello message
		System.out.println("Hello Java World");
	}// end declaration of main method

} // end declaration of utility class