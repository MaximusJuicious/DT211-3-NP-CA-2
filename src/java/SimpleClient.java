/*****************************************************************************/
/* Mark Deegan                                                               */
/* Java socket client and server examples taken from:                        */
/* http://www.tutorialspoint.com/java/java_networking.htm                    */
/*****************************************************************************/

// we want everything to be in the ie.dit.mark package
package ie.dit.mark;


// needed for 
import java.net.*;
import java.io.*;

/** SimpleClient class creates a socket to connect to a server and pass a message */
public class SimpleClient
{
	/* main method 
	a: reads the server name and the port from the command line
	b: 
	*/
	public static void main(String [] args)
	{
		/** serverName stores a copy of the first command-line argument */
		String serverName = args[0];
      
		// port stores the (integer) conversion of the second command-line argument
		int port = Integer.parseInt(args[1]);

		// several of the following commands could throw an exception so we wrap
		// the statements in a single try-catch block
		try
		{
			// display a Connecting to... message
			System.out.println("Connecting to " + serverName + " on port " + port);
            
            // create a new socket called client which connects directly to the
            // server on the specified port
            Socket client = new Socket(serverName, port);
			
			// print a connected message
			System.out.println("Just connected to " + client.getRemoteSocketAddress());
                      
            // create an OutputStream to write data to the socket client
			OutputStream outToServer = client.getOutputStream();

			// create a DataOutputStream to write data to the server
			// via the outToServer OutputStream
			DataOutputStream out = new DataOutputStream(outToServer);

			// write data to the DataOutputStream out
			out.writeUTF("Hello from " + client.getLocalSocketAddress());
			
			// create an InputStream to receive data from the socket client
			InputStream inFromServer = client.getInputStream();
			
			// create a DatInput Stream to receive data from the 
			// InputStream inFromServer
			DataInputStream in = new DataInputStream(inFromServer);
			
		// debug message
		System.out.println("About to read from Server");
			
			// print the response received from the server
			// System.out.println("Server says " + in.readUTF());

			// close the client socket
			client.close();

      } // end of try block
      
      catch(IOException e)
      {
         e.printStackTrace();
      } // end of catch to catch any sort of exception
      
   } // end of declaration of main method
} // end declaration of SimpleClient class