#!/bin/bash

# Mark Deegan
# Tue 10 Feb 2015 09:53:42 GMT
# DT211-3
# Network Programming
# February 2015
# CA-2
#
# script to run Hello java application
# this script should be run from the src/bash directory of the project

# this runs ie.dit.mark.Hello from the bin directory
java -cp ../../bin ie.dit.deegan.mark.Hello
