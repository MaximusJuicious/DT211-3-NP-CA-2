#!/bin/bash

# Mark Deegan
# Tue 10 Feb 2015 09:53:42 GMT
# DT211-3
# Network Programming
# February 2015
# CA-2
#
# script to run HelloWorld.o
# this script should be run from the src/bash directory of the project

# this runs HelloWorld.o from the bin directory
../../bin/HelloWorld.o
