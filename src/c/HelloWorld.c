/****************************************************/
/*  Mark Deegan                                     */
/*                                                  */
/* DT211-3                                          */
/* Network Perogramming                             */
/* February 2015                                    */
/* CA-2                                             */ 
/* Hello World Program (C)                          */
/*                                                  */
/****************************************************/


#include <stdio.h> /* necessary for printf function */

int main(int argc, char** argv)
/* start declaration of main function               */          
{ 	
	printf("Hello World \n");
}
/* end   declaration of main function               */          