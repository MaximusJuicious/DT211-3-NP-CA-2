 #include <stdio.h>  
 #include <signal.h>  
 #include <unistd.h>
  
/*  
 * number of times the handle will run:  
 */  
 volatile int breakflag = 3;  
  
 void handle(int sig) {  
    printf("You woke me.\n");  
    --breakflag;  
    alarm(2);  
 }  
  
 int main() {
 	printf("We're going to wait a while\n");
 	
    signal(SIGALRM, handle);  
    alarm(4);  
    
    while(breakflag) { 
    	printf("Going to sleep for 10 seconds\n");
    	sleep(10); 
    }  
    
    printf("Ah, I give up!\n");  
    return 0;  
 }  