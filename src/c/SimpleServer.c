#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>				// required for exit function
#include <strings.h>			// required for bzero function
#include <unistd.h>				// required for read and write functions

void error(char *msg) {
	perror(msg);
    exit(0);
}

int main(int argc, char **argv) {
	int serversockfd, clientsockfd, portnum;
	unsigned int clilen;
	char buffer[256];
	struct sockaddr_in server_addr, client_addr;	
	int n;
     
     serversockfd = socket(AF_INET, SOCK_STREAM, 0);

     bzero((char *) &server_addr, sizeof(server_addr));

     portnum = atoi(argv[1]);
     
     server_addr.sin_family = AF_INET;
     
     server_addr.sin_addr.s_addr = INADDR_ANY;
     
     server_addr.sin_port = htons(portnum);
     
     bind(serversockfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
 
 	listen(serversockfd,5);
 	clilen = sizeof(client_addr);
  	clientsockfd = accept(serversockfd, (struct sockaddr *) &client_addr, &clilen);
 	 		
 	bzero(buffer,256);

	n = read(clientsockfd,buffer,255);
	
	n = write(clientsockfd,"I got your message",18);
}